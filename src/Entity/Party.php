<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartyRepository")
 */
class Party
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $shareexp;

    /**
     * @ORM\Column(type="integer")
     */
    private $shareitem;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $leaderid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShareexp(): ?int
    {
        return $this->shareexp;
    }

    public function setShareexp(int $shareexp): self
    {
        $this->shareexp = $shareexp;

        return $this;
    }

    public function getShareitem(): ?int
    {
        return $this->shareitem;
    }

    public function setShareitem(int $shareitem): self
    {
        $this->shareitem = $shareitem;

        return $this;
    }

    public function getLeaderid(): ?string
    {
        return $this->leaderid;
    }

    public function setLeaderid(string $leaderid): self
    {
        $this->leaderid = $leaderid;

        return $this;
    }
}
