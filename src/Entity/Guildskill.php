<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GuildskillRepository")
 */
class Guildskill
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $guildid;

    /**
     * @ORM\Column(type="integer")
     */
    private $dataid;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;


    public function getGuildid(): ?int
    {
        return $this->guildid;
    }

    public function setGuildid(int $guildid): self
    {
        $this->guildid = $guildid;

        return $this;
    }

    public function getDataid(): ?int
    {
        return $this->dataid;
    }

    public function setDataid(int $dataid): self
    {
        $this->dataid = $dataid;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }
}
