<?php

namespace App\Repository;

use App\Entity\Guildname;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Guildname|null find($id, $lockMode = null, $lockVersion = null)
 * @method Guildname|null findOneBy(array $criteria, array $orderBy = null)
 * @method Guildname[]    findAll()
 * @method Guildname[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuildnameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Guildname::class);
    }

    // /**
    //  * @return Guildname[] Returns an array of Guildname objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Guildname
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
