<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GuildRepository")
 */
class Guild
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $guildname;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $leaderid;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="integer")
     */
    private $exp;

    /**
     * @ORM\Column(type="integer")
     */
    private $skillpoint;

    /**
     * @ORM\Column(type="string", length=160)
     */
    private $guildmessage;

    /**
     * @ORM\Column(type="integer")
     */
    private $gold;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGuildname(): ?string
    {
        return $this->guildname;
    }

    public function setGuildname(string $guildname): self
    {
        $this->guildname = $guildname;

        return $this;
    }

    public function getLeaderid(): ?string
    {
        return $this->leaderid;
    }

    public function setLeaderid(string $leaderid): self
    {
        $this->leaderid = $leaderid;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getExp(): ?int
    {
        return $this->exp;
    }

    public function setExp(int $exp): self
    {
        $this->exp = $exp;

        return $this;
    }

    public function getSkillpoint(): ?int
    {
        return $this->skillpoint;
    }

    public function setSkillpoint(int $skillpoint): self
    {
        $this->skillpoint = $skillpoint;

        return $this;
    }

    public function getGuildmessage(): ?string
    {
        return $this->guildmessage;
    }

    public function setGuildmessage(string $guildmessage): self
    {
        $this->guildmessage = $guildmessage;

        return $this;
    }

    public function getGold(): ?int
    {
        return $this->gold;
    }

    public function setGold(int $gold): self
    {
        $this->gold = $gold;

        return $this;
    }
}
