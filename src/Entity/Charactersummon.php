<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CharactersummonRepository")
 */
class Charactersummon
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $characterid;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $dataid;

    /**
     * @ORM\Column(type="float")
     */
    private $summonremainsduration;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="integer")
     */
    private $exp;

    /**
     * @ORM\Column(type="integer")
     */
    private $currenthp;

    /**
     * @ORM\Column(type="integer")
     */
    private $currentmp;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateat;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCharacterid(): ?string
    {
        return $this->characterid;
    }

    public function setCharacterid(string $characterid): self
    {
        $this->characterid = $characterid;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDataid(): ?int
    {
        return $this->dataid;
    }

    public function setDataid(int $dataid): self
    {
        $this->dataid = $dataid;

        return $this;
    }

    public function getSummonremainsduration(): ?float
    {
        return $this->summonremainsduration;
    }

    public function setSummonremainsduration(float $summonremainsduration): self
    {
        $this->summonremainsduration = $summonremainsduration;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getExp(): ?int
    {
        return $this->exp;
    }

    public function setExp(int $exp): self
    {
        $this->exp = $exp;

        return $this;
    }

    public function getCurrenthp(): ?int
    {
        return $this->currenthp;
    }

    public function setCurrenthp(int $currenthp): self
    {
        $this->currenthp = $currenthp;

        return $this;
    }

    public function getCurrentmp(): ?int
    {
        return $this->currentmp;
    }

    public function setCurrentmp(int $currentmp): self
    {
        $this->currentmp = $currentmp;

        return $this;
    }

    public function getCreateat(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setCreateat(\DateTimeInterface $createat): self
    {
        $this->createat = $createat;

        return $this;
    }

    public function getUpdateat(): ?\DateTimeInterface
    {
        return $this->updateat;
    }

    public function setUpdateat(\DateTimeInterface $updateat): self
    {
        $this->updateat = $updateat;

        return $this;
    }
}
