<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CharacteritemRepository")
 */
class Characteritem
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idx;

    /**
     * @ORM\Column(type="integer")
     */
    private $inventorytype;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $characterid;

    /**
     * @ORM\Column(type="integer")
     */
    private $dataid;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="integer")
     */
    private $equipslotindex;

    /**
     * @ORM\Column(type="float")
     */
    private $durability;

    /**
     * @ORM\Column(type="integer")
     */
    private $exp;

    /**
     * @ORM\Column(type="float")
     */
    private $lockremainsduration;

    /**
     * @ORM\Column(type="integer")
     */
    private $ammo;

    /**
     * @ORM\Column(type="text")
     */
    private $sockets;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateat;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIdx(): ?int
    {
        return $this->idx;
    }

    public function setIdx(int $idx): self
    {
        $this->idx = $idx;

        return $this;
    }

    public function getInventorytype(): ?int
    {
        return $this->inventorytype;
    }

    public function setInventorytype(int $inventorytype): self
    {
        $this->inventorytype = $inventorytype;

        return $this;
    }

    public function getCharacterid(): ?string
    {
        return $this->characterid;
    }

    public function setCharacterid(string $characterid): self
    {
        $this->characterid = $characterid;

        return $this;
    }

    public function getDataid(): ?int
    {
        return $this->dataid;
    }

    public function setDataid(int $dataid): self
    {
        $this->dataid = $dataid;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getEquipslotindex(): ?int
    {
        return $this->equipslotindex;
    }

    public function setEquipslotindex(int $equipslotindex): self
    {
        $this->equipslotindex = $equipslotindex;

        return $this;
    }

    public function getDurability(): ?float
    {
        return $this->durability;
    }

    public function setDurability(float $durability): self
    {
        $this->durability = $durability;

        return $this;
    }

    public function getExp(): ?int
    {
        return $this->exp;
    }

    public function setExp(int $exp): self
    {
        $this->exp = $exp;

        return $this;
    }

    public function getLockremainsduration(): ?float
    {
        return $this->lockremainsduration;
    }

    public function setLockremainsduration(float $lockremainsduration): self
    {
        $this->lockremainsduration = $lockremainsduration;

        return $this;
    }

    public function getAmmo(): ?int
    {
        return $this->ammo;
    }

    public function setAmmo(int $ammo): self
    {
        $this->ammo = $ammo;

        return $this;
    }

    public function getSockets(): ?string
    {
        return $this->sockets;
    }

    public function setSockets(string $sockets): self
    {
        $this->sockets = $sockets;

        return $this;
    }

    public function getCreateat(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setCreateat(\DateTimeInterface $createat): self
    {
        $this->createat = $createat;

        return $this;
    }

    public function getUpdateat(): ?\DateTimeInterface
    {
        return $this->updateat;
    }

    public function setUpdateat(\DateTimeInterface $updateat): self
    {
        $this->updateat = $updateat;

        return $this;
    }
}
