<?php

namespace App\Repository;

use App\Entity\Charactersummon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Charactersummon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Charactersummon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Charactersummon[]    findAll()
 * @method Charactersummon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharactersummonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Charactersummon::class);
    }

    // /**
    //  * @return Charactersummon[] Returns an array of Charactersummon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Charactersummon
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
