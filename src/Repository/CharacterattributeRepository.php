<?php

namespace App\Repository;

use App\Entity\Characterattribute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Characterattribute|null find($id, $lockMode = null, $lockVersion = null)
 * @method Characterattribute|null findOneBy(array $criteria, array $orderBy = null)
 * @method Characterattribute[]    findAll()
 * @method Characterattribute[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharacterattributeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Characterattribute::class);
    }

    // /**
    //  * @return Characterattribute[] Returns an array of Characterattribute objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Characterattribute
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
