<?php

namespace App\Repository;

use App\Entity\Characterskillusage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Characterskillusage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Characterskillusage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Characterskillusage[]    findAll()
 * @method Characterskillusage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharacterskillusageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Characterskillusage::class);
    }

    // /**
    //  * @return Characterskillusage[] Returns an array of Characterskillusage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Characterskillusage
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
