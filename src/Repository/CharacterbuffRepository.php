<?php

namespace App\Repository;

use App\Entity\Characterbuff;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Characterbuff|null find($id, $lockMode = null, $lockVersion = null)
 * @method Characterbuff|null findOneBy(array $criteria, array $orderBy = null)
 * @method Characterbuff[]    findAll()
 * @method Characterbuff[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharacterbuffRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Characterbuff::class);
    }

    // /**
    //  * @return Characterbuff[] Returns an array of Characterbuff objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Characterbuff
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
