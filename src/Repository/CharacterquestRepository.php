<?php

namespace App\Repository;

use App\Entity\Characterquest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Characterquest|null find($id, $lockMode = null, $lockVersion = null)
 * @method Characterquest|null findOneBy(array $criteria, array $orderBy = null)
 * @method Characterquest[]    findAll()
 * @method Characterquest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharacterquestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Characterquest::class);
    }

    // /**
    //  * @return Characterquest[] Returns an array of Characterquest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Characterquest
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
