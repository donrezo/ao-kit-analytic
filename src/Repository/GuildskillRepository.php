<?php

namespace App\Repository;

use App\Entity\Guildskill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Guildskill|null find($id, $lockMode = null, $lockVersion = null)
 * @method Guildskill|null findOneBy(array $criteria, array $orderBy = null)
 * @method Guildskill[]    findAll()
 * @method Guildskill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuildskillRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Guildskill::class);
    }

    // /**
    //  * @return Guildskill[] Returns an array of Guildskill objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Guildskill
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
