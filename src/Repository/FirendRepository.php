<?php

namespace App\Repository;

use App\Entity\Firend;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Firend|null find($id, $lockMode = null, $lockVersion = null)
 * @method Firend|null findOneBy(array $criteria, array $orderBy = null)
 * @method Firend[]    findAll()
 * @method Firend[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FirendRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Firend::class);
    }

    // /**
    //  * @return Firend[] Returns an array of Firend objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Firend
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
