<?php

namespace App\Repository;

use App\Entity\Guildrole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Guildrole|null find($id, $lockMode = null, $lockVersion = null)
 * @method Guildrole|null findOneBy(array $criteria, array $orderBy = null)
 * @method Guildrole[]    findAll()
 * @method Guildrole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuildroleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Guildrole::class);
    }

    // /**
    //  * @return Guildrole[] Returns an array of Guildrole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Guildrole
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
