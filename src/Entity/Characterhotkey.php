<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CharacterhotkeyRepository")
 */
class Characterhotkey
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $characterid;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $hotkeyid;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $relateid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateat;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCharacterid(): ?string
    {
        return $this->characterid;
    }

    public function setCharacterid(string $characterid): self
    {
        $this->characterid = $characterid;

        return $this;
    }

    public function getHotkeyid(): ?string
    {
        return $this->hotkeyid;
    }

    public function setHotkeyid(string $hotkeyid): self
    {
        $this->hotkeyid = $hotkeyid;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getRelateid(): ?string
    {
        return $this->relateid;
    }

    public function setRelateid(string $relateid): self
    {
        $this->relateid = $relateid;

        return $this;
    }

    public function getCreateat(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setCreateat(\DateTimeInterface $createat): self
    {
        $this->createat = $createat;

        return $this;
    }

    public function getUpdateat(): ?\DateTimeInterface
    {
        return $this->updateat;
    }

    public function setUpdateat(\DateTimeInterface $updateat): self
    {
        $this->updateat = $updateat;

        return $this;
    }
}
