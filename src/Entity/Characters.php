<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CharactersRepository")
 */
class Characters
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $userid;

    /**
     * @ORM\Column(type="integer")
     */
    private $dataid;

    /**
     * @ORM\Column(type="integer")
     */
    private $entityid;

    /**
     * @ORM\Column(type="integer")
     */
    private $factionid;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $charactername;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="integer")
     */
    private $exp;

    /**
     * @ORM\Column(type="integer")
     */
    private $currenthp;

    /**
     * @ORM\Column(type="integer")
     */
    private $currentmp;

    /**
     * @ORM\Column(type="integer")
     */
    private $currentstamina;

    /**
     * @ORM\Column(type="integer")
     */
    private $currentfood;

    /**
     * @ORM\Column(type="integer")
     */
    private $currentwater;

    /**
     * @ORM\Column(type="integer")
     */
    private $equipweaponset;

    /**
     * @ORM\Column(type="integer")
     */
    private $statpoint;

    /**
     * @ORM\Column(type="integer")
     */
    private $skillpoint;

    /**
     * @ORM\Column(type="integer")
     */
    private $gold;

    /**
     * @ORM\Column(type="integer")
     */
    private $partyid;

    /**
     * @ORM\Column(type="integer")
     */
    private $guildid;

    /**
     * @ORM\Column(type="integer")
     */
    private $guildrole;

    /**
     * @ORM\Column(type="integer")
     */
    private $sharedguildexp;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $currentmapname;

    /**
     * @ORM\Column(type="float")
     */
    private $currentpositionx;

    /**
     * @ORM\Column(type="float")
     */
    private $currentpositiony;

    /**
     * @ORM\Column(type="float")
     */
    private $currentpositionz;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $respawnmapname;

    /**
     * @ORM\Column(type="float")
     */
    private $respawnpositionx;

    /**
     * @ORM\Column(type="float")
     */
    private $respawnpositiony;

    /**
     * @ORM\Column(type="float")
     */
    private $respawnpositionz;

    /**
     * @ORM\Column(type="integer")
     */
    private $mountdataid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateat;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUserid(): ?string
    {
        return $this->userid;
    }

    public function setUserid(string $userid): self
    {
        $this->userid = $userid;

        return $this;
    }

    public function getDataid(): ?int
    {
        return $this->dataid;
    }

    public function setDataid(int $dataid): self
    {
        $this->dataid = $dataid;

        return $this;
    }

    public function getEntityid(): ?int
    {
        return $this->entityid;
    }

    public function setEntityid(int $entityid): self
    {
        $this->entityid = $entityid;

        return $this;
    }

    public function getFactionid(): ?int
    {
        return $this->factionid;
    }

    public function setFactionid(int $factionid): self
    {
        $this->factionid = $factionid;

        return $this;
    }

    public function getCharactername(): ?string
    {
        return $this->charactername;
    }

    public function setCharactername(string $charactername): self
    {
        $this->charactername = $charactername;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getExp(): ?int
    {
        return $this->exp;
    }

    public function setExp(int $exp): self
    {
        $this->exp = $exp;

        return $this;
    }

    public function getCurrenthp(): ?int
    {
        return $this->currenthp;
    }

    public function setCurrenthp(int $currenthp): self
    {
        $this->currenthp = $currenthp;

        return $this;
    }

    public function getCurrentmp(): ?int
    {
        return $this->currentmp;
    }

    public function setCurrentmp(int $currentmp): self
    {
        $this->currentmp = $currentmp;

        return $this;
    }

    public function getCurrentstamina(): ?int
    {
        return $this->currentstamina;
    }

    public function setCurrentstamina(int $currentstamina): self
    {
        $this->currentstamina = $currentstamina;

        return $this;
    }

    public function getCurrentfood(): ?int
    {
        return $this->currentfood;
    }

    public function setCurrentfood(int $currentfood): self
    {
        $this->currentfood = $currentfood;

        return $this;
    }

    public function getCurrentwater(): ?int
    {
        return $this->currentwater;
    }

    public function setCurrentwater(int $currentwater): self
    {
        $this->currentwater = $currentwater;

        return $this;
    }

    public function getEquipweaponset(): ?int
    {
        return $this->equipweaponset;
    }

    public function setEquipweaponset(int $equipweaponset): self
    {
        $this->equipweaponset = $equipweaponset;

        return $this;
    }

    public function getStatpoint(): ?int
    {
        return $this->statpoint;
    }

    public function setStatpoint(int $statpoint): self
    {
        $this->statpoint = $statpoint;

        return $this;
    }

    public function getSkillpoint(): ?int
    {
        return $this->skillpoint;
    }

    public function setSkillpoint(int $skillpoint): self
    {
        $this->skillpoint = $skillpoint;

        return $this;
    }

    public function getGold(): ?int
    {
        return $this->gold;
    }

    public function setGold(int $gold): self
    {
        $this->gold = $gold;

        return $this;
    }

    public function getPartyid(): ?int
    {
        return $this->partyid;
    }

    public function setPartyid(int $partyid): self
    {
        $this->partyid = $partyid;

        return $this;
    }

    public function getGuildid(): ?int
    {
        return $this->guildid;
    }

    public function setGuildid(int $guildid): self
    {
        $this->guildid = $guildid;

        return $this;
    }

    public function getGuildrole(): ?int
    {
        return $this->guildrole;
    }

    public function setGuildrole(int $guildrole): self
    {
        $this->guildrole = $guildrole;

        return $this;
    }

    public function getSharedguildexp(): ?int
    {
        return $this->sharedguildexp;
    }

    public function setSharedguildexp(int $sharedguildexp): self
    {
        $this->sharedguildexp = $sharedguildexp;

        return $this;
    }

    public function getCurrentmapname(): ?string
    {
        return $this->currentmapname;
    }

    public function setCurrentmapname(string $currentmapname): self
    {
        $this->currentmapname = $currentmapname;

        return $this;
    }

    public function getCurrentpositionx(): ?float
    {
        return $this->currentpositionx;
    }

    public function setCurrentpositionx(float $currentpositionx): self
    {
        $this->currentpositionx = $currentpositionx;

        return $this;
    }

    public function getCurrentpositiony(): ?float
    {
        return $this->currentpositiony;
    }

    public function setCurrentpositiony(float $currentpositiony): self
    {
        $this->currentpositiony = $currentpositiony;

        return $this;
    }

    public function getCurrentpositionz(): ?float
    {
        return $this->currentpositionz;
    }

    public function setCurrentpositionz(float $currentpositionz): self
    {
        $this->currentpositionz = $currentpositionz;

        return $this;
    }

    public function getRespawnmapname(): ?string
    {
        return $this->respawnmapname;
    }

    public function setRespawnmapname(string $respawnmapname): self
    {
        $this->respawnmapname = $respawnmapname;

        return $this;
    }

    public function getRespawnpositionx(): ?float
    {
        return $this->respawnpositionx;
    }

    public function setRespawnpositionx(float $respawnpositionx): self
    {
        $this->respawnpositionx = $respawnpositionx;

        return $this;
    }

    public function getRespawnpositiony(): ?float
    {
        return $this->respawnpositiony;
    }

    public function setRespawnpositiony(float $respawnpositiony): self
    {
        $this->respawnpositiony = $respawnpositiony;

        return $this;
    }

    public function getRespawnpositionz(): ?float
    {
        return $this->respawnpositionz;
    }

    public function setRespawnpositionz(float $respawnpositionz): self
    {
        $this->respawnpositionz = $respawnpositionz;

        return $this;
    }

    public function getMountdataid(): ?int
    {
        return $this->mountdataid;
    }

    public function setMountdataid(int $mountdataid): self
    {
        $this->mountdataid = $mountdataid;

        return $this;
    }

    public function getCreateat(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setCreateat(\DateTimeInterface $createat): self
    {
        $this->createat = $createat;

        return $this;
    }

    public function getUpdateat(): ?\DateTimeInterface
    {
        return $this->updateat;
    }

    public function setUpdateat(\DateTimeInterface $updateat): self
    {
        $this->updateat = $updateat;

        return $this;
    }
}
