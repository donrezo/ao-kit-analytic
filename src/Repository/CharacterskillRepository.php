<?php

namespace App\Repository;

use App\Entity\Characterskill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Characterskill|null find($id, $lockMode = null, $lockVersion = null)
 * @method Characterskill|null findOneBy(array $criteria, array $orderBy = null)
 * @method Characterskill[]    findAll()
 * @method Characterskill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharacterskillRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Characterskill::class);
    }

    // /**
    //  * @return Characterskill[] Returns an array of Characterskill objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Characterskill
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
