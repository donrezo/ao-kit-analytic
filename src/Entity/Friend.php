<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FriendRepository")
 */
class Friend
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $characterid1;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $characterid2;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCharacterid1(): ?string
    {
        return $this->characterid1;
    }

    public function setCharacterid1(string $characterid1): self
    {
        $this->characterid1 = $characterid1;

        return $this;
    }

    public function getCharacterid2(): ?string
    {
        return $this->characterid2;
    }

    public function setCharacterid2(string $characterid2): self
    {
        $this->characterid2 = $characterid2;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->createat = $datetime;

        return $this;
    }

    public function getUpdateat(): ?\DateTimeInterface
    {
        return $this->updateat;
    }

    public function setUpdateat(\DateTimeInterface $updateat): self
    {
        $this->updateat = $updateat;

        return $this;
    }
}
