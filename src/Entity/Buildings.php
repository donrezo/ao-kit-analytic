<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BuildingsRepository")
 */
class Buildings
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $parentid;

    /**
     * @ORM\Column(type="integer")
     */
    private $dataid;

    /**
     * @ORM\Column(type="integer")
     */
    private $currenthp;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $mapname;

    /**
     * @ORM\Column(type="float")
     */
    private $positionx;

    /**
     * @ORM\Column(type="float")
     */
    private $positiony;

    /**
     * @ORM\Column(type="float")
     */
    private $positionz;

    /**
     * @ORM\Column(type="float")
     */
    private $rotationx;

    /**
     * @ORM\Column(type="float")
     */
    private $rotationy;

    /**
     * @ORM\Column(type="float")
     */
    private $rotationz;

    /**
     * @ORM\Column(type="boolean")
     */
    private $islocked;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $lockpassword;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $creatorid;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $creatorname;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateat;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getParentid(): ?string
    {
        return $this->parentid;
    }

    public function setParentid(string $parentid): self
    {
        $this->parentid = $parentid;

        return $this;
    }

    public function getDataid(): ?int
    {
        return $this->dataid;
    }

    public function setDataid(int $dataid): self
    {
        $this->dataid = $dataid;

        return $this;
    }

    public function getCurrenthp(): ?int
    {
        return $this->currenthp;
    }

    public function setCurrenthp(int $currenthp): self
    {
        $this->currenthp = $currenthp;

        return $this;
    }

    public function getMapname(): ?string
    {
        return $this->mapname;
    }

    public function setMapname(string $mapname): self
    {
        $this->mapname = $mapname;

        return $this;
    }

    public function getPositionx(): ?float
    {
        return $this->positionx;
    }

    public function setPositionx(float $positionx): self
    {
        $this->positionx = $positionx;

        return $this;
    }

    public function getPositiony(): ?float
    {
        return $this->positiony;
    }

    public function setPositiony(float $positiony): self
    {
        $this->positiony = $positiony;

        return $this;
    }

    public function getPositionz(): ?float
    {
        return $this->positionz;
    }

    public function setPositionz(float $positionz): self
    {
        $this->positionz = $positionz;

        return $this;
    }

    public function getRotationx(): ?float
    {
        return $this->rotationx;
    }

    public function setRotationx(float $rotationx): self
    {
        $this->rotationx = $rotationx;

        return $this;
    }

    public function getRotationy(): ?float
    {
        return $this->rotationy;
    }

    public function setRotationy(float $rotationy): self
    {
        $this->rotationy = $rotationy;

        return $this;
    }

    public function getRotationz(): ?float
    {
        return $this->rotationz;
    }

    public function setRotationz(float $rotationz): self
    {
        $this->rotationz = $rotationz;

        return $this;
    }

    public function getIslocked(): ?bool
    {
        return $this->islocked;
    }

    public function setIslocked(bool $islocked): self
    {
        $this->islocked = $islocked;

        return $this;
    }

    public function getLockpassword(): ?string
    {
        return $this->lockpassword;
    }

    public function setLockpassword(string $lockpassword): self
    {
        $this->lockpassword = $lockpassword;

        return $this;
    }

    public function getCreatorid(): ?string
    {
        return $this->creatorid;
    }

    public function setCreatorid(string $creatorid): self
    {
        $this->creatorid = $creatorid;

        return $this;
    }

    public function getCreatorname(): ?string
    {
        return $this->creatorname;
    }

    public function setCreatorname(string $creatorname): self
    {
        $this->creatorname = $creatorname;

        return $this;
    }

    public function getCreateat(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setCreateat(\DateTimeInterface $createat): self
    {
        $this->createat = $createat;

        return $this;
    }

    public function getUpdatetime(): ?\DateTimeInterface
    {
        return $this->updateat;
    }

    public function setUpdatetime(\DateTimeInterface $updatetime): self
    {
        $this->updateat = $updatetime;

        return $this;
    }
}
