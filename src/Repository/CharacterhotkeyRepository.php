<?php

namespace App\Repository;

use App\Entity\Characterhotkey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Characterhotkey|null find($id, $lockMode = null, $lockVersion = null)
 * @method Characterhotkey|null findOneBy(array $criteria, array $orderBy = null)
 * @method Characterhotkey[]    findAll()
 * @method Characterhotkey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharacterhotkeyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Characterhotkey::class);
    }

    // /**
    //  * @return Characterhotkey[] Returns an array of Characterhotkey objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Characterhotkey
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
