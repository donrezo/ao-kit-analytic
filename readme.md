## MMOKIT - WEB KIT ##

This is kit for web apps base on SURIYUN MMORPG KIT (UNITY). It covers all entities in version 1.53. You can build base on this your own apps.

## PREREQUISITE ##

https://assetstore.unity.com/packages/templates/systems/mmorpg-kit-2d-3d-survival-110188
~~~
PHP 7.4.2
MARIADB or MYSQL with already imported sql from unit pack
SYMFONY CLI -> https://symfony.com/download
~~~

## INSTALL ##

~~~
cd project_dir
composer install
~~~


## CONFIGURATION ##

To add db edit .env file:

~~~
DATABASE_URL=mysql://root:root@127.0.0.1:3306/mmorpgtemplate
~~~

## RUN ##

~~~
cd project_dir
symfony serve
~~~

### BASIC ENDPOINTS ###

MMOKIT - Easy Admin Panel:
~~~
http://127.0.0.1:8000/mmokit
~~~
![Image of MMOKIT](https://i.ibb.co/DCTvnVj/mmokit.png)


TEST Controller:
~~~
http://127.0.0.1:8000/test
~~~
