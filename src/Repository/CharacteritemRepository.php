<?php

namespace App\Repository;

use App\Entity\Characteritem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Characteritem|null find($id, $lockMode = null, $lockVersion = null)
 * @method Characteritem|null findOneBy(array $criteria, array $orderBy = null)
 * @method Characteritem[]    findAll()
 * @method Characteritem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharacteritemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Characteritem::class);
    }

    // /**
    //  * @return Characteritem[] Returns an array of Characteritem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Characteritem
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
