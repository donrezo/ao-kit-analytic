<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserLoginRepository")
 */
class userlogin
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $password;

    /**
     * @ORM\Column(type="integer")
     */
    private $gold;

    /**
     * @ORM\Column(type="integer")
     */
    private $cash;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $email;

    /**
     * @ORM\Column(type="integer")
     */
    private $authtype;

    /**
     * @ORM\Column(type="string", length=36)
     */
    private $accesstoken;

    /**
     * @ORM\Column(type="integer")
     */
    private $userlevel;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateat;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getGold(): ?int
    {
        return $this->gold;
    }

    public function setGold(int $gold): self
    {
        $this->gold = $gold;

        return $this;
    }

    public function getCash(): ?int
    {
        return $this->cash;
    }

    public function setCash(int $cash): self
    {
        $this->cash = $cash;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAuthType(): ?bool
    {
        return $this->authtype;
    }

    public function setAuthType(bool $authtype): self
    {
        $this->authtype = $authtype;

        return $this;
    }

    public function getAccessToken(): ?string
    {
        return $this->accesstoken;
    }

    public function setAccessToken(string $accessToken): self
    {
        $this->accesstoken = $accessToken;

        return $this;
    }

    public function getUserLevel(): ?bool
    {
        return $this->userlevel;
    }

    public function setUserLevel(bool $userLevel): self
    {
        $this->userlevel = $userLevel;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setCreateAt(\DateTimeInterface $createdAt): self
    {
        $this->createat = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateat;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateat = $updateAt;

        return $this;
    }
}
