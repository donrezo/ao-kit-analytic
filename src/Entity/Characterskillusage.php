<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CharacterskillusageRepository")
 */
class Characterskillusage
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $characterid;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $dataid;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="float")
     */
    private $cooldownremainsduration;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateat;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCharacterid(): ?string
    {
        return $this->characterid;
    }

    public function setCharacterid(string $characterid): self
    {
        $this->characterid = $characterid;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDataid(): ?int
    {
        return $this->dataid;
    }

    public function setDataid(int $dataid): self
    {
        $this->dataid = $dataid;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getCooldownremainsduration(): ?float
    {
        return $this->cooldownremainsduration;
    }

    public function setCooldownremainsduration(float $cooldownremainsduration): self
    {
        $this->cooldownremainsduration = $cooldownremainsduration;

        return $this;
    }

    public function getCreateat(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setCreateat(\DateTimeInterface $createat): self
    {
        $this->createat = $createat;

        return $this;
    }

    public function getUpdateat(): ?\DateTimeInterface
    {
        return $this->updateat;
    }

    public function setUpdateat(\DateTimeInterface $updateat): self
    {
        $this->updateat = $updateat;

        return $this;
    }
}
