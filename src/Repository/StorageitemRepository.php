<?php

namespace App\Repository;

use App\Entity\Storageitem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Storageitem|null find($id, $lockMode = null, $lockVersion = null)
 * @method Storageitem|null findOneBy(array $criteria, array $orderBy = null)
 * @method Storageitem[]    findAll()
 * @method Storageitem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StorageitemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Storageitem::class);
    }

    // /**
    //  * @return Storageitem[] Returns an array of Storageitem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Storageitem
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
