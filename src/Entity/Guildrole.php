<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GuildroleRepository")
 */
class Guildrole
{


    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $guildid;

    /**
     * @ORM\Column(type="integer")
     */
    private $guildrole;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $caninvite;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cankick;

    /**
     * @ORM\Column(type="integer")
     */
    private $shareexppercentage;


    public function getGuildid(): ?int
    {
        return $this->guildid;
    }

    public function setGuildid(int $guildid): self
    {
        $this->guildid = $guildid;

        return $this;
    }

    public function getGuildrole(): ?int
    {
        return $this->guildrole;
    }

    public function setGuildrole(int $guildrole): self
    {
        $this->guildrole = $guildrole;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCaninvite(): ?bool
    {
        return $this->caninvite;
    }

    public function setCaninvite(bool $caninvite): self
    {
        $this->caninvite = $caninvite;

        return $this;
    }

    public function getCankick(): ?bool
    {
        return $this->cankick;
    }

    public function setCankick(bool $cankick): self
    {
        $this->cankick = $cankick;

        return $this;
    }

    public function getShareexppercentage(): ?int
    {
        return $this->shareexppercentage;
    }

    public function setShareexppercentage(int $shareexppercentage): self
    {
        $this->shareexppercentage = $shareexppercentage;

        return $this;
    }
}
